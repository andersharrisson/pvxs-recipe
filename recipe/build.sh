#!/bin/bash

cat <<EOF > configure/RELEASE.local
EPICS_BASE=${EPICS_BASE}
EOF

make

install bin/${EPICS_HOST_ARCH}/* ${PREFIX}/bin/
install lib/${EPICS_HOST_ARCH}/libpvxs.* ${PREFIX}/lib/
install -d ${PREFIX}/include/pvxs
install -m 0664 src/pvxs/*.h ${PREFIX}/include/pvxs/
install -m 0664 src/pvxs/versionNum.h@ ${PREFIX}/include/pvxs/versionNum.h
